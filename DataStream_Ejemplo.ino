//Librerias Necesarias (ESP8266.WiFi debe incluirse despues de FirebaseESP8266)
#include "FirebaseESP8266.h"
#include <ESP8266WiFi.h>

// Constantes: Información de la base de datos y el HotSpot
#define FIREBASE_HOST "URL_BaseDeDatos_Firebase" // Ejemplo: intro-database-test.firebaseio.com
#define FIREBASE_AUTH "Secreto_BaseDeDatos" // Este se obtiene en las configuraciones de la base de datos de Firebase
#define WIFI_SSID "Nombre_Red_WiFi"
#define WIFI_PASSWORD "Clave_Red_WiFI"

// Objeto FirebaseESP8266
FirebaseData firebaseData;

// Variables
unsigned long sendDataPrevMillis = 0;
String path = "/DataStream/Ejemplo";
uint16_t count = 0;

void setup()
{
  // Inicialización del Serial
  Serial.begin(115200);

  // Conexión al HotSpot
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Intentando conexión con Dispositivo WiFi: " + String(WIFI_SSID));
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(300);
  }
  Serial.println();
  Serial.print("Conectado con IP: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  // Conexión a la base de datos
  Serial.print("Intentando conexión con base de datos: " + String(FIREBASE_HOST));
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  Firebase.reconnectWiFi(true);
  if (!Firebase.beginStream(firebaseData, path))
  {
    Serial.println("------------------------------------");
    Serial.println("No se pudo iniciar el streaming de datos...");
    Serial.println("ERROR: " + firebaseData.errorReason());
    Serial.println("------------------------------------");
    Serial.println();
    delay(1000);
  }
  else
  {
    Serial.println("Se ha iniciado el streaming de datos... ");
  }
}

void loop()
{
  // Envio de datos cada 20 Segundos
  if (millis() - sendDataPrevMillis > 20000)
  {
    // Actualización de variables
    sendDataPrevMillis = millis();
    count++;

    // Ejemplo de envio de dato de tipo STRING
    Serial.println("------------------------------------");
    if (Firebase.setString(firebaseData, path + "/Nivel_de_CO2/Dato" + String(count), "Alto" ))
    {
      Serial.println("Dato de Nivel de CO2 enviado con éxito");
      Serial.print("Valor enviado: ");
      // Verificación del Mensaje enviado
      if (firebaseData.dataType() == "int")
        Serial.println(firebaseData.
        intData());
      else if (firebaseData.dataType() == "float")
        Serial.println(firebaseData.floatData(), 5);
      else if (firebaseData.dataType() == "double")
        printf("%.9lf\n", firebaseData.doubleData());
      else if (firebaseData.dataType() == "boolean")
        Serial.println(firebaseData.boolData() == 1 ? "true" : "false");
      else if (firebaseData.dataType() == "string")
        Serial.println(firebaseData.stringData());
      else if (firebaseData.dataType() == "json")
        Serial.println(firebaseData.jsonData());
      Serial.println("------------------------------------");
      Serial.println();
    }
    else
    {
      Serial.println("Fallo envio de datos");
      Serial.println("ERROR: " + firebaseData.errorReason());
      Serial.println("------------------------------------");
      Serial.println();
    }

    // Ejemplo de envio de dato de tipo DOUBLE
    Serial.println("------------------------------------");
    if (Firebase.setDouble(firebaseData, path + "/Longitud/Dato" + String(count), -74.0817500 ))
    {
      Serial.println("Dato de Nivel de Longitud enviado con éxito");
      Serial.print("Valor enviado: ");
      // Verificación del Mensaje enviado
      if (firebaseData.dataType() == "int")
        Serial.println(firebaseData.
        intData());
      else if (firebaseData.dataType() == "float")
        Serial.println(firebaseData.floatData(), 5);
      else if (firebaseData.dataType() == "double")
        printf("%.9lf\n", firebaseData.doubleData());
      else if (firebaseData.dataType() == "boolean")
        Serial.println(firebaseData.boolData() == 1 ? "true" : "false");
      else if (firebaseData.dataType() == "string")
        Serial.println(firebaseData.stringData());
      else if (firebaseData.dataType() == "json")
        Serial.println(firebaseData.jsonData());
      Serial.println("------------------------------------");
      Serial.println();
    }
    else
    {
      Serial.println("Fallo envio de datos");
      Serial.println("ERROR: " + firebaseData.errorReason());
      Serial.println("------------------------------------");
      Serial.println();
    }

  // Otras verificaciones
  if (firebaseData.streamTimeout())
  {
    Serial.println("Stream timeout, resume streaming...");
    Serial.println();
  }

  if (firebaseData.streamAvailable())
  {
    Serial.println("------------------------------------");
    Serial.println("Stream Data available...");
    Serial.println("STREAM PATH: " + firebaseData.streamPath());
    Serial.println("EVENT PATH: " + firebaseData.dataPath());
    Serial.println("DATA TYPE: " + firebaseData.dataType());
    Serial.println("EVENT TYPE: " + firebaseData.eventType());
    Serial.print("VALUE: ");
    if (firebaseData.dataType() == "int")
      Serial.println(firebaseData.intData());
    else if (firebaseData.dataType() == "float")
      Serial.println(firebaseData.floatData());
    else if (firebaseData.dataType() == "boolean")
      Serial.println(firebaseData.boolData() == 1 ? "true" : "false");
    else if (firebaseData.dataType() == "string")
      Serial.println(firebaseData.stringData());
    else if (firebaseData.dataType() == "json")
      Serial.println(firebaseData.jsonData());
    Serial.println("------------------------------------");
    Serial.println();
  }
  }
}
